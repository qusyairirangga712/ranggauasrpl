@extends('layouts.app')

@section('content')
<div class="container">


            <div class="content">
                <div class="row">
                    @forelse ($videos as $video)

                    <div class="col-md-4">
                        <a href="/{{$video->youtube_id}}">
                        <div class="panel panel-default">
                            <img width="100%"
src="https://img.youtube.com/vi/{{$video->youtube_id}}/0.jpg"
class="media-object" >
                            <div class="panel-heading">
                            {{$video->title}}
                            </div>
                        </div>
                        </a>
                    </div>
                    @empty

                    @endforelse
                </div>
                    {{$videos->links()}}
            </div>
</div>
@endsection
